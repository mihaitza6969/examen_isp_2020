package mihai.ghiran.examen2020;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;







public class Examen extends JFrame {
	
	JLabel nr1,nr2;
	JTextField number1,number2;
	JTextArea result;
    JButton calculate;
    

	
	Examen(){
		setTitle("File write");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	init();
        setSize(300,235);
        setVisible(true);
		
	}
	
void init() {
    	
    	this.setLayout(null);
        int width=80;int height = 20;
        
        result = new JTextArea();
        result.setBounds(10,100,150,30);
        
        result.setEditable(false);

        nr1 = new JLabel("Insert nr 1 ");
        nr1.setBounds(10, 20, width, height);

        nr2 = new JLabel("Insert nr 2 ");
        nr2.setBounds(10, 50,width, height);

        number1 = new JTextField();
        number1.setBounds(70,20,width, height);

        number2 = new JTextField();
        number2.setBounds(70,60,width, height);

        calculate = new JButton("Calculate");
        calculate.setBounds(180,100,100, height);
        
        calculate.addActionListener(new Calculate());

        add(nr1);add(nr2);add(number1);add(number2);add(result);add(calculate);
    	
    }

	public static void main(String[] args) {
		new Examen();
}
	
	class Calculate implements ActionListener{
  	  
          public void actionPerformed(ActionEvent e) {
          	
        	  
        	  int nr1 = Integer.parseInt(number1.getText());
          	  int nr2 = Integer.parseInt(number2.getText());
          	  int res =nr1*nr2; 
          	 
          	Examen.this.result.append(Integer.toString(res ));
                
          }
	
}
}


//Ex2
/*

public class L{
	private int a;
	public void meh(X x) {
		
	}	
}

public class X{
	
}

public class M{
	private B b;
}

public class B{
	public metB() {}
		
	
}

public class A extends C{
	
	public M m;
	
	public metA() {}
	A(){
		this.m = new M();
		
	}
}

public class C
*/
